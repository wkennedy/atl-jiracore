defaults:
main.yml

handlers:
main.yml

tasks:
main.yml

templates:
jira-config.properties.j2
jiracore-config.properties.j2
jiracore.service.j2
